<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css" />
		<script src="http://code.jquery.com/jquery-1.8.2.js"></script>
		<script src="http://code.jquery.com/ui/1.9.0/jquery-ui.js"></script>
		<link rel="stylesheet" href="/resources/demos/style.css" />
		<script>
			$(function() {
				$("#tabs").tabs().addClass("ui-tabs-vertical ui-helper-clearfix");
				$("#tabs li").removeClass("ui-corner-top").addClass("ui-corner-left");
			});
		</script>
		<script>
			//$(document).ready(function(){
			//    $("input[name='']:radio").change(function() {
			//	alert("Option changed!");
			//    });
			//});
		</script>
		<style>
			html,body {
				height:100%
			}
			tr:hover{background:silver}
			.leftContainer{
		  		float:left;
		  		width:15%;
		  		height:100%;
		  		background:silver;
		  		padding:none;
  			}
			.ui-tabs-vertical {
				width: 55em;
			}
			.ui-tabs-vertical .ui-tabs-nav {
				padding: .2em .1em .2em .2em;
				float: left;
				width: 12em;
			}
			.ui-tabs-vertical .ui-tabs-nav li {
				clear: left;
				width: 100%;
				border-bottom-width: 1px !important;
				border-right-width: 0 !important;
				margin: 0 -1px .2em 0;
			}
			.ui-tabs-vertical .ui-tabs-nav li a {
				display: block;
			}
			.ui-tabs-vertical .ui-tabs-nav li.ui-tabs-active {
				padding-bottom: 0;
				padding-right: .1em;
				border-right-width: 1px;
				border-right-width: 1px;
			}
			.ui-tabs-vertical .ui-tabs-panel {
				padding: 1em;
				float: right;
				width: 40em;
			}
		</style>
</head>
<?php
	$dbName = 'test';
	$dbHost = '216.249.119.100';
	$dbUser = 'devdba';
	$dbPw = '#PlantDb!';
	$mysqli = new mysqli($dbHost, $dbUser,$dbPw,$dbName);
	if (mysqli_connect_errno($mysqli)) 
	{
	    echo "Failed to connect to MySQL: " . $mysqli->connect_error;
	}
	$results = mysqli_query($mysqli, "SELECT title,category_id FROM categories");
	$categories = $results->fetch_all(MYSQL_NUM);
?>

<body>
<div id="searchbar" class="leftContainer">
	Search by scientific name or common name <input type="text" name="searchByName">
	<input type="submit" value="Submit">
</div>
 <div id="tabs" style="width:42.5%;float:left">
 	<ul>
	 <?php
	 	 $i = 1;
	     foreach ($categories as $row)
		 {
		 	echo "<li><a href=\"#tabs-" . $i . "\">" . $row[0] . "</a></li>";
			$i++;
		 }
	?>
	</ul>
	<?php
		$i = 1;
		foreach ($categories as $row)
		{
			$display = "";		
			$query = mysqli_query($mysqli, "SELECT DISTINCT category_id,value FROM plantcategories where category_id = " . $row[1]);
			$values = $query->fetch_all(MYSQL_NUM);	
			$display = "<div id=\"tabs-" . $i . "\" style=\"width:42.5%\">"
			. "<h2>" . $row[0] . "</h2> <p>";
			foreach ($values as $value)
			{
				$display = $display . "<input type=\"radio\" value=\" . $value[1] . \">" . $value[1] . "<br>";
			} 
			$display = $display . "</p></div>";
			echo $display;
			$i++;
		}
	?>
</div>
<div id="results" style="float:right;left-margin:42.6%;width:40%;text-align:center;overflow: scroll">
	<h1>Results</h1>
	<table cellspacing="0" border="0">
		<?php
			$query2 = mysqli_query($mysqli, "SELECT DISTINCT plant_id,location FROM pictures");
			$pictures = $query2->fetch_all(MYSQL_NUM);
			$prev_id = 0;
			foreach ($pictures as $row)
			{
				if ($prev_id == 0)
				{
					echo "<tr>"
					. "<td><img src=\"" . $row[1] . "\" height=\"120\";width=\"120\"/></td>";
				}
				else if ($prev_id == $row[0])
				{
					echo "<td><img src=\"" . $row[1] . "\" height=\"120\";width=\"120\"/></td>";
				}
				else 
				{
					echo "</tr>"
					. "<tr>"
					. "<td><img src=\"" . $row[1] . "\" height=\"120\";width=\"120\"/></td>";
				}	
				$prev_id = $row[0];
			}
			echo "</tr>";
		?>
	</table>
</div>
</body>
</html>