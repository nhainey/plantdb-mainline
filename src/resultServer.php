<?php
require_once("config.php");
// Prevent sending anything to people that are just loading the page
if($_SERVER['REQUEST_METHOD'] != "POST" && $_SERVER['REQUEST_METHOD'] != "GET")
{
    die();
}

// -- Globals -- //
$CATEGORY_TAG = "categoryFilterForm-";

// NOTE: The "Unknown error." is here just for debugging, make sure to remove it in production
$result = array("status"=>"error", "error"=>"Unknown error.");

if($_SERVER['REQUEST_METHOD'] == "POST")
{
    switch($_POST['action'])
    {
        
    }
}
else if($_SERVER['REQUEST_METHOD'] == "GET")
{
    switch($_GET['action'])
    {
        case "updateResults":
            $result["status"] = "ok";
            
            // Prepare the query (NOTE: It doesn't do partial matching in the value portion!)
            $prepStmt = $mysqli->prepare("SELECT plant_id FROM PlantCategories WHERE category_id=? AND value=?");
            $plants = array();
            
            foreach($_POST as $key => $value)
            {
                // Make sure that we don't check values we don't need to
                if(strpos($key, $CATEGORY_TAG) === false)
                {
                    continue;
                }
                
                $categoryID = substr($key, strlen()-1 /* substr() is 0-based */);
                
                $prepStmt->bind_param("is", $categoryID, $value);
                $prepStmt->execute();
                $res = $prepStmt->get_results();
                $val = $res->fetch_assoc();
                $plants[] = $val['plant_id'];
            }
            
            // TODO: Get actual plant data from DB
            
            
            $result["value"] = array(
                array(
                    "id"=>1,
                    "familyName" => "Family Name",
                    "genusName" => "Genus Name",
                    "speciesName" => "Species Name",
                    "commonName" => array("Common Name"),
                    "pictures" => array("images/sunflower.jpg")
                ),
                array(
                    "id"=>2,
                    "familyName" => "Some Family",
                    "genusName" => "Some Genus",
                    "speciesName" => "Some Species",
                    "commonName" => array("Plant Name", "Another Name"),
                    "pictures" => array("images/sunflower.jpg", "images/Sunflower-With-Bee.jpg")
                )
            );
            break;
        case "getCategories":
            $result["status"] = "ok";
            
            $res = $mysqli->query("SELECT id, title FROM Categories");
            if($res == false)
            {
                $result["message"] = "Invalid query: ".$mysqli->error;
                // TODO: Force error state and skip out
            }
            
            while($row = $res->fetch_assoc())
            {
                $result['value'][] = array($row['id'], $row['title']);
            }
            
            $res->free();
            break;
        case "getCategoryFilter":
            $result["status"] = "ok";
            
            // Get the category
            $stmt = $mysqli->prepare("SELECT * FROM Category WHERE id = ? LIMIT 1");
            $stmt->bind_param("i", $_GET['id']);
            $stmt->execute();
            $res = $stmt->get_result();
            
            // Get the options
            $stmt = $mysqli->prepare("SELECT * FROM CategoryOptions WHERE categories_id = ?");
            $stmt->bind_param("i", $_GET['id']);
            $stmt->execute();
            $optionRes = $stmt->get_result();
            
            $row = $res->fetch_assoc();
            // TODO: Check for failure
            
            $result["value"] = array(
                "id" => $row['id'],
                "name" => $row['title'],
                "text" => $row['description'],
                "type" => $row['type'],
            );
            // Only give options if there are some to give
            if($optionRes->num_rows >= 1)
            {
                while($row = $optionRes->fetch_assoc())
                {
                    $result["value"]["options"][] = array($row['id'], $row['option']);
                }
            }
            break;
    }
}

echo json_encode($result);
die();
?>
