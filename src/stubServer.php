<?php
/*
 * This file is just a stub. It's used merely for showing various test results on the main page.
 * DO NOT use this file for production. Make one that actually uses the database!
 */

$result = array("status"=>"error", "error"=>"Unknown error.");

if($_SERVER['REQUEST_METHOD'] == "POST")
{
    switch($_POST['action'])
    {
        
    }
}
else if($_SERVER['REQUEST_METHOD'] == "GET")
{
    switch($_GET['action'])
    {
        case "updateResults":
            $result["status"] = "ok";
            
            $result["value"] = array(
                array(
                    "id"=>1,
                    "familyName" => "Family Name",
                    "genusName" => "Genus Name",
                    "speciesName" => "Species Name",
                    "commonName" => array("Common Name"),
                    "pictures" => array("images/sunflower.jpg")
                ),
                array(
                    "id"=>2,
                    "familyName" => "Some Family",
                    "genusName" => "Some Genus",
                    "speciesName" => "Some Species",
                    "commonName" => array("Plant Name", "Another Name"),
                    "pictures" => array("images/sunflower.jpg", "images/Sunflower-With-Bee.jpg")
                )
            );
            break;
        case "getCategories":
            $result["status"] = "ok";
            
            $result["value"] = array(
                array(
                    "id"=>1,
                    "name" => "Petal Color"
                ),
                array(
                    "id"=>2,
                    "name" => "Petal Count"
                ),
                array(
                    "id"=>3,
                    "name" => "Stamen Count"
                )
            );
            break;
        case "getCategoryFilter":
            $result["status"] = "ok";
            
            switch($_GET['id'])
            {
                case 1:
                    $result["value"] = array(
                        "id" => 1,
                        "name" => "Petal Color",
                        "text" => "Please select the petal colors: ",
                        "type" => "dropdown",
                        "options" => array(
                            array("1", "Red"),
                            array("2", "Blue"),
                            array("3", "Purple"),
                            array("4", "Orange"),
                            array("5", "Yellow"),
                            array("6", "Violet"),
                            array("7", "Green"),
                            array("8", "Other")
                        )
                    );
                    break;
                case 2:
                    $result["value"] = array(
                        "id" => 2,
                        "name" => "Petal Count",
                        "text" => "Please enter the number of petals: ",
                        "type" => "text"
                    );
                    break;
                case 3:
                    $result["value"] = array(
                        "id" => 3,
                        "name" => "Stamen Count",
                        "text" => "Please enter the number of stamens: ",
                        "type" => "text"
                    );
                    break;
            }
            break;
    }
}

echo json_encode($result);
die();
?>
